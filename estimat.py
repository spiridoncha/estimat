#!/usr/bin/env python
# PYTHON_ARGCOMPLETE_OK
import demandimport
with demandimport.enabled():
    import argcomplete
    import argparse
    import gc
    import json
    import os
    import re
    import h5py
    import numpy as np
    from functools import partial
    from itertools import product
    from lib import utils
    from lib import data_processing as dp

class_weight = {
    0 : 1,
    1 : 1,
    2 : 1,
    3 : 1
}

def custom_metrics(y_true, y_pred):
    from keras import backend as K
    pred = K.argmax(y_pred, axis=-1)
    true = K.argmax(y_true, axis=-1)
    tp_idxs = K.less_equal(true, 0)
    tp_idxs = K.cast(tp_idxs, 'int16')
    eq = K.equal(pred, true)
    eq = K.cast(eq, 'int16')
    return K.sum(eq * tp_idxs, axis=None)

def custom_loss(y_true, y_pred, weights):
    from keras import backend as K
    nb_cl = len(weights)
    final_mask = K.zeros_like(y_pred[:, 0])
    y_pred_max = K.max(y_pred, axis=1)
    y_pred_max = K.reshape(y_pred_max, (K.shape(y_pred)[0], 1))
    y_pred_max_mat = K.equal(y_pred, y_pred_max)
    for c_p, c_t in product(range(nb_cl), range(nb_cl)):
        final_mask += (weights[c_t, c_p] * y_pred_max_mat[:, c_p] * y_true[:, c_t])
    return K.mean(K.square(y_true - y_pred), axis=-1) * final_mask

def make_parser():
    desc = ''
    def is_dir(st):
        if not os.path.isdir(st):
            msg = "Directory '{0}' don't exist".format(st)
            raise argparse.ArgumentTypeError(msg)
        return st

    def is_reg_file(st):
        if not os.path.isfile(st):
            msg = "File '{0}' don't exist".format(st)
            raise argparse.ArgumentTypeError(msg)
        return st

    default_epochs_amount = 125
    default_batch_size = 1
    default_verbose_mode = 0
    default_validation_split = 0.1
    default_model_name = 'model'
    default_input_model_name = default_model_name + '.h5'
    default_folds = 10

    validation_split_help = 'Number(range [0,1]) - part of dataset for validation'
    epochs_help = 'Number of epochs for learning'
    batch_size_help = 'Size of mini-batches for learning'
    verbose_help = 'Level of info for print'
    no_shuffle_help = 'No shuffle data before training'
    export_help = 'Export markup for project'
    folds_help = 'Number of folds("KFold")'

    parser = argparse.ArgumentParser(description=desc)

    parser.add_argument('--config', metavar='config_file', type=is_reg_file, default=os.path.expanduser('~/.estimat.conf'), help="Default - '~/.estimat.conf'")
    parser.add_argument('-p', '--project', metavar='project_name', type=str, required=True, help='Name of project(it''s key from config file')

    # TODO this option should be moved from global options
    parser.add_argument('--earlystop', action='store_true', default=False, help='Activate early stopping method for learning process')

    subparsers = parser.add_subparsers(help='')

    features_help = 'show features'
    parser_features = subparsers.add_parser('features', help=features_help)

    visualize_help = 'visualize graph'
    parser_visualize = subparsers.add_parser('visualize', help=visualize_help)

    fit_help = 'Learning model'
    parser_fit = subparsers.add_parser('fit', help=fit_help)

    predict_help = 'Preidct with learned model'
    parser_predict = subparsers.add_parser('predict', help=predict_help)

    double_predict_help = 'Preidct with learned model'
    parser_double_predict = subparsers.add_parser('double-predict', help=double_predict_help)

    cross_validation_help = 'Cross validation on project'
    parser_cross_validation = subparsers.add_parser('cross-validation', help=cross_validation_help)

    ensemble_help = 'Command for predict/fit/cross-validate with ensemble neural network'
    parser_ensemble = subparsers.add_parser('ensemble', help=ensemble_help)

    ensemble_subparser = parser_ensemble.add_subparsers(help='')

    ensemble_predict_help = 'Predict labels on project with ensemble nn'
    parser_ensemble_predict = ensemble_subparser.add_parser('predict', help=ensemble_predict_help)

    ensemble_fit_help = 'Fit ensemble nn'
    parser_ensemble_fit = ensemble_subparser.add_parser('fit', help=ensemble_fit_help)

    ensemble_cross_validation_help = 'Cross-validate on hand-markuped and auto-markuped data'
    parser_ensemble_cross_validation = ensemble_subparser.add_parser('cross-validation', help=ensemble_cross_validation_help)

    parser_features.add_argument('-v', '--verbose', type=int, choices=[0, 1, 2], default=default_verbose_mode, help=verbose_help)
    parser_features.set_defaults(func=features)

    parser_visualize.set_defaults(func=visualize_graph)

    parser_fit.add_argument('-o', '--output', type=str, default=default_model_name, help='Name of file for saving model')
    parser_fit.add_argument('-v', '--verbose', type=int, choices=[0, 1, 2], default=default_verbose_mode, help=verbose_help)
    parser_fit.add_argument('--validation-split', type=float, default=default_validation_split, help=validation_split_help)
    parser_fit.add_argument('--epochs', type=int, default=default_epochs_amount, help=epochs_help)
    parser_fit.add_argument('--batch-size', type=int, default=default_batch_size, help=batch_size_help)
    parser_fit.add_argument('--no-shuffle', action='store_true', default=False, help=no_shuffle_help)
    parser_fit.set_defaults(func=fit)

    parser_predict.add_argument('-i', '--input', type=is_reg_file, default=default_input_model_name, help='Name of file with model')
    parser_predict.add_argument('-v', '--verbose', type=int, choices=[0, 1, 2], default=default_verbose_mode, help=verbose_help)
    parser_predict.add_argument('--batch-size', type=int, default=default_batch_size, help=batch_size_help)
    parser_predict.add_argument('--export', type=str, default=None, help=export_help)
    parser_predict.set_defaults(func=predict)

    parser_double_predict.add_argument('--input-with-one-path', type=is_reg_file, default=default_input_model_name, help='')
    parser_double_predict.add_argument('--input-without-one-path', type=is_reg_file, default=default_input_model_name, help='')
    parser_double_predict.add_argument('-v', '--verbose', type=int, choices=[0, 1, 2], default=default_verbose_mode, help=verbose_help)
    parser_double_predict.add_argument('--batch-size', type=int, default=default_batch_size, help=batch_size_help)
    parser_double_predict.add_argument('--export', type=str, default=None, help=export_help)
    parser_double_predict.set_defaults(func=double_predict)

    #TODO cross_validation options
    parser_cross_validation.add_argument('-v', '--verbose', type=int, choices=[0, 1, 2], default=default_verbose_mode, help=verbose_help)
    parser_cross_validation.add_argument('--epochs', type=int, default=default_epochs_amount, help=epochs_help)
    parser_cross_validation.add_argument('--batch-size', type=int, default=default_batch_size, help=batch_size_help)
    parser_cross_validation.add_argument('--folds', type=int, default=default_folds, help=folds_help)
    parser_cross_validation.set_defaults(func=crossValidation)

    parser_ensemble_predict.add_argument('--input', type=str, default='ensemblenn', help='Name of dir with models')
    parser_ensemble_predict.add_argument('--export', type=str, default=None, help='')
    parser_ensemble_predict.add_argument('-v', '--verbose', type=int, choices=[0, 1, 2], default=default_verbose_mode, help=verbose_help)
    parser_ensemble_predict.add_argument('--batch-size', type=int, default=default_batch_size, help=batch_size_help)
    parser_ensemble_predict.set_defaults(func=ensemblePredict)

    parser_ensemble_fit.add_argument('-o', '--output', type=str, default='ensemblenn', help='Name of dir for saving models')
    parser_ensemble_fit.add_argument('-v', '--verbose', type=int, choices=[0, 1, 2], default=default_verbose_mode, help=verbose_help)
    parser_ensemble_fit.add_argument('--validation-split', type=float, default=default_validation_split, help=validation_split_help)
    parser_ensemble_fit.add_argument('--epochs', type=int, default=default_epochs_amount, help=epochs_help)
    parser_ensemble_fit.add_argument('--batch-size', type=int, default=default_batch_size, help=batch_size_help)
    parser_ensemble_fit.add_argument('--no-shuffle', action='store_true', default=False, help=no_shuffle_help)
    parser_ensemble_fit.add_argument('--size', type=int, default=10, help='Number of training models')
    parser_ensemble_fit.set_defaults(func=ensembleFit)

    parser_ensemble_cross_validation.add_argument('-v', '--verbose', type=int, choices=[0, 1, 2], default=default_verbose_mode, help=verbose_help)
    parser_ensemble_cross_validation.add_argument('--epochs', type=int, default=default_epochs_amount, help=epochs_help)
    parser_ensemble_cross_validation.add_argument('--batch-size', type=int, default=default_batch_size, help=batch_size_help)
    parser_ensemble_cross_validation.add_argument('--folds', type=int, default=default_folds, help=folds_help)
    parser_ensemble_cross_validation.set_defaults(func=ensembleCrossValidation)

    argcomplete.autocomplete(parser)
    return parser

# lsts - lists of equal length
def syncShuffle(*lsts):
    idxs = list(range(len(lsts[0])))
    np.random.shuffle(idxs)
    return [lst[idxs] for lst in list(lsts)]

def createModel(args, in_shape, in_rec_shape, output_dim, suffix=''):
    from keras.models import Model
    from keras.layers import Dense, Activation, Merge, Input, Dropout
    from keras.layers import concatenate
    from keras.layers.recurrent import LSTM
    from keras import optimizers
    from keras import metrics
    from keras import losses
    from keras.callbacks import EarlyStopping
    from keras.callbacks import ModelCheckpoint
    from keras import backend as K
    # from keras.callbacks import TensorBoard

    # K._LEARNING_PHASE = tf.constant(0)
    K.set_learning_phase(1)

    nonRecurrent_input = Input(shape=in_shape, name='1')
    # nonRecurrent_1 = Dense(30, activation='relu', name='nonRecurrent_dense1')(nonRecurrent_input)
    # nonRecurrent_2 = Dense(50, activation='relu', name='nonRecurrent_dense2')(nonRecurrent_1)

    nonRecurrent_2_t = Dense(60, activation='relu', name='2')(nonRecurrent_input)
    nonRecurrent_2 = Dropout(0.5, name='3')(nonRecurrent_2_t)

    recurrent_input = Input(shape=(None, in_rec_shape), name='4')
    recurrent = LSTM(16, name='9')(recurrent_input)

    # recurrent_out = Dense(24, activation='relu', name='dense_after_LSTM')(recurrent)
    recurrent_out_t = Dense(32, activation='relu', name='5')(recurrent)
    recurrent_out = Dropout(0.5, name='6')(recurrent_out_t)

    end_1 = concatenate([nonRecurrent_2, recurrent_out], name='7')
    output = Dense(output_dim, activation='softmax', name='8')(end_1)

    model = Model(inputs=[nonRecurrent_input, recurrent_input], outputs=output)

    # w_array = np.array([
    #     [1.0, 1.1, 1.2, 1.4, 1.4],
    #     [1.1, 1.0, 1.1, 1.4, 1.4],
    #     [1.2, 1.1, 1.0, 1.1, 1.2],
    #     [1.4, 1.4, 1.1, 1.0, 1.1],
    #     [1.4, 1.4, 1.2, 1.1, 1.0],
    # ])
    # loss_func = partial(custom_loss, weights=w_array)
    # loss_func.__name__ = 'custom_loss'
    # opt = optimizers.SGD(lr=0.005)
    opt = optimizers.SGD(lr=0.01)
    acc = metrics.categorical_accuracy
    model.compile(loss=losses.categorical_crossentropy, optimizer=opt, metrics=[acc, custom_metrics])
    callbacks = []
    callbacks.append(ModelCheckpoint('model-' + suffix + '.h5', monitor='val_loss', save_best_only=True))
    return model, callbacks

def visualize_graph(args):
    from keras.utils.vis_utils import model_to_dot

    project = args['project']
    project_options = utils.readConfigFile(args['config'], project)

    project_markup_base = project_options['project_markup_base']
    project_markup_dir = project_options['project_markup_dir']
    checkers_base = project_options['checkers_base']
    output_dim = 4

    raw_data, targets = dp.getDataFromBase(project_markup_base, project_markup_dir)
    raw_data, rec_data, _ = dp.rawToNP(raw_data, targets, checkers_base, output_dim)

    print(raw_data.shape, rec_data.shape)
    model, _ = createModel(args, raw_data.shape[1:], rec_data.shape[2], output_dim)
    dot_model = model_to_dot(model, show_shapes=True)
    dot_model.write('dot_model.gv')

def visualize(history, save_base_name, with_val=False):
    import matplotlib.pyplot as plt
    dpi = 200
    def visualizeLoss(history, save_file):
        plt.plot(history.history['loss'])
        if with_val:
            plt.plot(history.history['val_loss'])
        plt.title('model loss')
        plt.ylabel('loss')
        plt.xlabel('epoch')
        if with_val:
            plt.legend(['train', 'test'], loc='upper left')
        else:
            plt.legend(['train'], loc='upper left')
        plt.savefig(save_file, dpi=dpi)
        plt.close()

    def visualizeAcc(history, save_file):
        plt.plot(history.history['categorical_accuracy'])
        if with_val:
            plt.plot(history.history['val_categorical_accuracy'])
        plt.title('model accuracy')
        plt.ylabel('categorical_accuracy')
        plt.xlabel('epoch')
        if with_val:
            plt.legend(['train', 'test'], loc='upper left')
        else:
            plt.legend(['train'], loc='upper left')
        plt.savefig(save_file, dpi=dpi)
        plt.close()

    def visualizeCustom(history, save_file):
        plt.plot(history.history['custom_metrics'])
        if with_val:
            plt.plot(history.history['val_custom_metrics'])
        plt.title('model custom metrics')
        plt.ylabel('custom')
        plt.xlabel('epoch')
        if with_val:
            plt.legend(['train', 'test'], loc='upper left')
        else:
            plt.legend(['train'], loc='upper left')
        plt.savefig(save_file, dpi=dpi)
        plt.close()

    visualizeLoss(history, 'loss-' + save_base_name + '.png')
    visualizeAcc(history, 'acc-' + save_base_name + '.png')
    visualizeCustom(history, 'custom-' + save_base_name + '.png')

def features(args):
    project = args['project']
    project_options = utils.readConfigFile(args['config'], project)

    project_markup_base = project_options['project_markup_base']
    project_markup_dir = project_options['project_markup_dir']
    checkers_base = project_options['checkers_base']
    output_dim = 4
    raw_data, targets = dp.getDataFromBase(project_markup_base, project_markup_dir)
    hashes = [entry['issue_hash_content_of_line_in_context'] for entry in raw_data]
    raw_data, rec_data, targets = dp.rawToNP(raw_data, targets, checkers_base, output_dim)
    np.set_printoptions(precision=5, suppress=True)
    for i in zip(raw_data, targets, hashes):
        print(i[2])
        print(i[0], i[1])

def fit(args):
    project = args['project']
    project_options = utils.readConfigFile(args['config'], project)

    project_markup_base = project_options['project_markup_base']
    project_markup_dir = project_options['project_markup_dir']
    checkers_base = project_options['checkers_base']
    model_file = args['output']
    output_dim = 4

    raw_data, targets = dp.getDataFromBase(project_markup_base, project_markup_dir)
    # hashes = [entry['issue_hash_content_of_line_in_context'] for entry in raw_data]
    raw_data, rec_data, targets = dp.rawToNP(raw_data, targets, checkers_base, output_dim)

    if not args['no_shuffle']:
        raw_data, rec_data, targets = syncShuffle(raw_data, rec_data, targets)
    print(raw_data.shape, rec_data.shape)
    model, callbacks = createModel(args, raw_data.shape[1:], rec_data.shape[2], output_dim)
    history = model.fit([raw_data, rec_data], targets, batch_size=args['batch_size'], validation_split=args['validation_split'], epochs=args['epochs'], class_weight=class_weight, shuffle=True, callbacks=callbacks, verbose=args['verbose'])
    visualize(history, 'fit_' + model_file, True)
    model.save(model_file + '.h5')

def ensembleFit(args):
    from keras import backend as K
    from keras.callbacks import TensorBoard

    project = args['project']
    project_options = utils.readConfigFile(args['config'], project)

    project_markup_base = project_options['project_markup_base']
    project_markup_dir = project_options['project_markup_dir']
    checkers_base = project_options['checkers_base']
    model_dir = args['output']
    ensemble_size = args['size']
    output_dim = 4

    raw_data, targets = dp.getDataFromBase(project_markup_base, project_markup_dir)
    # hashes = [entry['issue_hash_content_of_line_in_context'] for entry in raw_data]
    raw_data, rec_data, targets = dp.rawToNP(raw_data, targets, checkers_base, output_dim)

    if not args['no_shuffle']:
        raw_data, rec_data, targets = syncShuffle(raw_data, rec_data, targets)
    print(raw_data.shape, rec_data.shape)

    for i in range(ensemble_size):
        number_format = '{:0' + str(len(str(ensemble_size))) + 'd}'
        model_file_name = 'ensemble' + number_format.format(i)
        model, callbacks = createModel(args, raw_data.shape[1:], rec_data.shape[2], output_dim, number_format.format(i))
        callbacks.append(TensorBoard(log_dir='.logs' + number_format.format(i), histogram_freq=1, write_images=True))
        raw_data, rec_data, targets = syncShuffle(raw_data, rec_data, targets)
        history = model.fit([raw_data, rec_data], targets, batch_size=args['batch_size'], validation_split=args['validation_split'], epochs=args['epochs'], class_weight=class_weight, shuffle=True, callbacks=callbacks, verbose=args['verbose'])
        visualize(history, 'fit_' + model_file_name, True)
        model.save(os.path.join(model_dir, model_file_name + '.h5'))
        K.clear_session()

def export(args, project_markup_dir, proba, hashes):
    if args['export'] is not None:
        to_project = args['export']
        to_project_options = utils.readConfigFile(args['config'], to_project)
        to_project_markup_base = to_project_options['project_markup_base']
        to_project_markup_dir = to_project_options['project_markup_dir']
        predicted_json = {}
        predicted_json['targets'] = {}
        for i in zip(proba, hashes):
            predicted_json['targets'][i[1]] = {
                'data_file'       : i[1],
                'label'           : str(int(np.argmax(i[0])) - 2),
                'mark'            : 'auto',
                'predicted_label' : str(int(np.argmax(i[0])) - 2),
                'TPprob'          : {
                    'TP'    : float(i[0][0]),
                    'MTP'   : float(i[0][1]),
                    'UNK'   : float(i[0][2]),
                    'FP'    : float(i[0][3]),
                    'alpha' : utils.getAlpha()
                 }
            }

        old_json = {}
        with utils.open_checkers_base(to_project_markup_base) as old_json:
            for target in old_json['targets']:
                if predicted_json['targets'].get(target):
                    predicted_target = predicted_json['targets'][target]
                    copy_keys = None
                    if old_json['targets'][target]['mark'] == 'auto':
                        copy_keys = ['label', 'predicted_label', 'TPprob']
                    else:
                        copy_keys = ['predicted_label', 'TPprob']
                    for key in copy_keys:
                        old_json['targets'][target][key] = predicted_target[key]

def corr(prob_idx, true_idx):
    if true_idx in [0, 1]:
        return 1 if prob_idx in [0, 1] else 0
    elif true_idx in [3]:
        return 1 if prob_idx in [2, 3] else 0
    else:
        return 1 if prob_idx == 2 else 0

def predict(args):
    from keras.models import load_model
    from keras import backend as K
    np.set_printoptions(precision=5, suppress=True)

    project = args['project']
    project_options = utils.readConfigFile(args['config'], project)

    project_markup_base = project_options['project_markup_base']
    project_markup_dir = project_options['project_markup_dir']
    checkers_base = project_options['checkers_base']
    model_file = args['input']
    output_dim = 4

    raw_data, targets = dp.getDataFromBase(project_markup_base, project_markup_dir)
    hashes = [entry['issue_hash_content_of_line_in_context'] for entry in raw_data]
    raw_data, rec_data, targets = dp.rawToNP(raw_data, targets, checkers_base, output_dim, True)
    print(raw_data.shape, rec_data.shape)
    K.set_learning_phase(0)
    model = load_model(model_file, custom_objects={'custom_metrics': custom_metrics})
    proba = model.predict([raw_data, rec_data], batch_size=args['batch_size'], verbose=args['verbose'])
    with open('predict', 'w') as f:
        for i in zip(proba, hashes):
            print(i[1], i[0], file=f)
    export(args, project_markup_dir, proba, hashes)

def double_predict(args):
    from keras.models import load_model
    from keras import backend as K
    np.set_printoptions(precision=5, suppress=True)

    project = args['project']
    project_options = utils.readConfigFile(args['config'], project)

    project_markup_base = project_options['project_markup_base']
    project_markup_dir = project_options['project_markup_dir']
    checkers_base = project_options['checkers_base']
    model_file_path = args['input_with_one_path']
    model_file_nopath = args['input_without_one_path']
    output_dim = 4

    raw_data, targets = dp.getDataFromBase(project_markup_base, project_markup_dir)
    hashes = [entry['issue_hash_content_of_line_in_context'] for entry in raw_data]
    raw_data, rec_data, targets = dp.rawToNP(raw_data, targets, checkers_base, output_dim, True)
    print(raw_data.shape, rec_data.shape)
    K.set_learning_phase(0)
    model_path = load_model(model_file_path, custom_objects={'custom_metrics': custom_metrics})
    proba_path = model_path.predict([raw_data, rec_data], batch_size=args['batch_size'], verbose=args['verbose'])
    model_nopath = load_model(model_file_nopath, custom_objects={'custom_metrics': custom_metrics})
    proba_nopath = model_nopath.predict([raw_data, rec_data], batch_size=args['batch_size'], verbose=args['verbose'])
    with open('predict', 'w') as f:
        for i in zip(proba_nopath, hashes):
            print(i[0], i[1], file=f)
        merged_proba = []
        for i in zip(proba_path, proba_nopath):
            if i[0][0] >= 0.7:
                merged_proba.append(i[0])
            else:
                merged_proba.append(i[1])
    export(args, project_markup_dir, merged_proba, hashes)

def ensemblePredict(args):
    from keras.models import load_model
    from keras import backend as K
    np.set_printoptions(precision=5, suppress=True)

    project = args['project']
    project_options = utils.readConfigFile(args['config'], project)

    project_markup_base = project_options['project_markup_base']
    project_markup_dir = project_options['project_markup_dir']
    checkers_base = project_options['checkers_base']
    model_dir = args['input']
    output_dim = 4

    raw_data, targets = dp.getDataFromBase(project_markup_base, project_markup_dir)
    hashes = [entry['issue_hash_content_of_line_in_context'] for entry in raw_data]
    raw_data, rec_data, targets = dp.rawToNP(raw_data, targets, checkers_base, output_dim, True)
    print(raw_data.shape, rec_data.shape)
    probas = []
    # with os.scandir(model_dir) as it:
    #     for entry in it:
    #         if entry.is_file():
    #             model = load_model(entry.path)
    #             probas.append(model.predict([raw_data, rec_data], batch_size=args['batch_size'], verbose=args['verbose']))
    for path in os.listdir(model_dir):
        if os.path.isfile(os.path.join(model_dir, path)):
             K.set_learning_phase(0)
             model = load_model(os.path.join(model_dir, path), custom_objects={'custom_metrics': custom_metrics})
             probas.append(model.predict([raw_data, rec_data], batch_size=args['batch_size'], verbose=args['verbose']))
    proba = np.average(np.array(probas), axis=0)
    with open('predict', 'w') as f:
        for i in zip(proba, hashes):
            print(i[1], i[0], file=f)

    export(args, project_markup_dir, proba, hashes)

def crossValResults(args, proba, val_targets, val_hashes, filename, idx, history):
    data = {}
    if os.path.isfile(filename):
        with open(filename, 'r') as f:
            data = json.load(f)
    else:
        data["results"] = []
    piece = {
        "length" : len(proba),
        "correct" : int(sum(i[1][np.argmax(i[0])] for i in zip(proba, val_targets))),
        "almost_correct" : sum(corr(np.argmax(i[0]), np.argmax(i[1])) for i in zip(proba, val_targets)),
        "tp_amount" : sum(1 if np.argmax(i) == 0 else 0 for i in val_targets),
        "almost_tp_amount" : sum(1 if np.argmax(i) in (0, 1) else 0 for i in val_targets),
        "other_amount" : sum(1 if np.argmax(i) not in (0, 1) else 0 for i in val_targets),
        "tp_correct" : sum(1 if np.argmax(i[0]) == 0 and np.argmax(i[1]) == 0 else 0 for i in zip(proba, val_targets)),
        "almost_tp_correct" : sum(1 if np.argmax(i[0]) in (0, 1) and np.argmax(i[1]) in (0, 1) else 0 for i in zip(proba, val_targets)),
        "other_incorrect" : sum(1 if np.argmax(i[0]) not in (0, 1) and np.argmax(i[1]) in (0, 1) else 0 for i in zip(proba, val_targets)),
        "self" : {},
        "history" : {
            "loss" : history.history['loss'],
            # what if we doesn't have val_loss ?
            "val_loss" : history.history['val_loss'],
            "categorical_accuracy" : history.history['categorical_accuracy'],
            # what if we doesn't have val_acc ?
            "val_categorical_accuracy" : history.history['val_categorical_accuracy']
        }
    }
    for i in zip(proba, val_hashes, val_targets):
        a = list(map(lambda x: float(x), i[0]))
        piece['self'][i[1]] = {
            "probs" : a,
            "class" : int(np.argmax(i[0]) - 2)
        }
    data["results"].append(piece)
    with open(filename, 'w') as f:
        json.dump(data, f)

def crossValidation(args):
    from keras import backend as K
    from keras.callbacks import TensorBoard
    from collections import namedtuple
    from sklearn.model_selection import StratifiedKFold
    np.set_printoptions(precision=5, suppress=True)

    def validate(model, callbacks, val_data, train_data, test_set_idx):
        test_set_data = val_data.nonRec
        test_set_data_rec = val_data.rec
        test_set_targets = val_data.targets
        test_set_hashes = val_data.hashes
        train_set_data = train_data.nonRec
        train_set_data_rec = train_data.rec
        train_set_targets = train_data.targets
        train_set_hashes = train_data.hashes
        history = model.fit([train_set_data, train_set_data_rec], train_set_targets, batch_size=args['batch_size'], validation_data=([test_set_data, test_set_data_rec], test_set_targets), epochs=args['epochs'], class_weight=class_weight, shuffle=True, callbacks=callbacks, verbose=args['verbose'])
        visualize(history, 'cross-validate' + str(test_set_idx), True)
        proba = model.predict([test_set_data, test_set_data_rec], batch_size=args['batch_size'], verbose=args['verbose'])
        crossValResults(args, proba, test_set_targets, test_set_hashes, 'result-cross-validation', test_set_idx, history)

    project = args['project']
    project_options = utils.readConfigFile(args['config'], project)

    project_markup_base = project_options['project_markup_base']
    project_markup_dir = project_options['project_markup_dir']
    checkers_base = project_options['checkers_base']
    output_dim = 4
    parts_amount = args['folds']

    Data = namedtuple('Data', ['nonRec', 'rec', 'targets', 'hashes'])

    raw_data, targets = dp.getDataFromBase(project_markup_base, project_markup_dir)
    hashes = np.array([entry['issue_hash_content_of_line_in_context'] for entry in raw_data])
    raw_data, rec_data, targets = dp.rawToNP(raw_data, targets, checkers_base, output_dim)

    fold_targets = np.apply_along_axis(lambda x: np.argmax(x), -1, targets)
    skf = StratifiedKFold(n_splits=parts_amount)

    i = 0
    for train_idxs, val_idxs in skf.split(np.zeros(raw_data.shape[0]), fold_targets):
        number_format = '{:0' + str(len(str(parts_amount))) + 'd}'
        model, callbacks = createModel(args, raw_data.shape[1:], rec_data.shape[2], output_dim, number_format.format(i))
        callbacks.append(TensorBoard(log_dir='.logs-cross' + number_format.format(i), histogram_freq=1, write_images=True))

        nonrec_train_data = raw_data[train_idxs]
        rec_train_data = rec_data[train_idxs]
        train_targets = targets[train_idxs]
        train_hashes = hashes[train_idxs]

        nonrec_val_data = raw_data[val_idxs]
        rec_val_data = rec_data[val_idxs]
        val_targets = targets[val_idxs]
        val_hashes = hashes[val_idxs]

        val_data = Data(
            nonRec=nonrec_val_data,
            rec=rec_val_data,
            targets=val_targets,
            hashes=val_hashes
        )

        train_data = Data(
            nonRec=nonrec_train_data,
            rec=rec_train_data,
            targets=train_targets,
            hashes=train_hashes
        )

        validate(model, callbacks, val_data, train_data, i)
        i += 1
        K.clear_session()

def ensembleCrossValidation(args):
    from collections import namedtuple
    from keras import backend as K
    from keras.callbacks import TensorBoard
    from sklearn.model_selection import StratifiedKFold

    np.set_printoptions(precision=5, suppress=True)

    def splitHandAuto(data, targets):
        hand_raw_data = []
        hand_targets = []
        auto_raw_data = []
        auto_targets = []
        for dt in zip(data, targets):
            if dt[1]['mark'] == 'hand':
                hand_raw_data.append(dt[0])
                hand_targets.append(dt[1])
            else:
                auto_raw_data.append(dt[0])
                auto_targets.append(dt[1])
        return hand_raw_data, hand_targets, auto_raw_data, auto_targets

    def validate(model, callbacks, val_data, train_data, test_set_idx):
        test_set_data = val_data.nonRec
        test_set_data_rec = val_data.rec
        test_set_targets = val_data.targets
        test_set_hashes = val_data.hashes
        train_set_data = train_data.nonRec
        train_set_data_rec = train_data.rec
        train_set_targets = train_data.targets
        train_set_hashes = train_data.hashes
        history = model.fit([train_set_data, train_set_data_rec], train_set_targets, batch_size=args['batch_size'], validation_data=([test_set_data, test_set_data_rec], test_set_targets), epochs=args['epochs'], class_weight=class_weight, shuffle=True, verbose=args['verbose'], callbacks=callbacks)

        number_format = '{:0' + str(len(str(args['folds']))) + 'd}'
        visualize(history, 'ensemble-cross-validate' + number_format.format(test_set_idx), True)
        K.set_learning_phase(0)
        proba = model.predict([test_set_data, test_set_data_rec], batch_size=args['batch_size'], verbose=args['verbose'])
        crossValResults(args, proba, test_set_targets, test_set_hashes, 'result-ensemble-cross-validation', test_set_idx, history)

    project = args['project']
    project_options = utils.readConfigFile(args['config'], project)

    project_markup_base = project_options['project_markup_base']
    project_markup_dir = project_options['project_markup_dir']
    checkers_base = project_options['checkers_base']
    output_dim = 4
    parts_amount = args['folds']

    Data = namedtuple('Data', ['nonRec', 'rec', 'targets', 'hashes'])

    raw_data, targets = dp.getDataFromBase(project_markup_base, project_markup_dir)
    hand_raw_data, hand_targets, auto_raw_data, auto_targets = splitHandAuto(raw_data, targets)
    if not len(hand_raw_data):
        msg = 'Error: need hand markuped data in markup base'
        utils.goodbye(msg)

    hand_hashes = np.array([entry['issue_hash_content_of_line_in_context'] for entry in hand_raw_data])
    auto_hashes = np.array([entry['issue_hash_content_of_line_in_context'] for entry in auto_raw_data])
    hand_raw_data, hand_rec_data, hand_targets = dp.rawToNP(hand_raw_data, hand_targets, checkers_base, output_dim)
    auto_raw_data, auto_rec_data, auto_targets = dp.rawToNP(auto_raw_data, auto_targets, checkers_base, output_dim)

    fold_targets = np.apply_along_axis(lambda x: np.argmax(x), -1, hand_targets)
    skf = StratifiedKFold(n_splits=parts_amount)

    i = 0
    for train_idxs, val_idxs in skf.split(np.zeros(hand_raw_data.shape[0]), fold_targets):
        number_format = '{:0' + str(len(str(parts_amount))) + 'd}'
        model, callbacks = createModel(args, hand_raw_data.shape[1:], hand_rec_data.shape[2], output_dim, number_format.format(i))
        callbacks.append(TensorBoard(log_dir='.logs-cross' + number_format.format(i), histogram_freq=1, write_images=True))

        nonrec_train_data = hand_raw_data[train_idxs]
        rec_train_data = hand_rec_data[train_idxs]
        train_targets = hand_targets[train_idxs]
        train_hashes = hand_hashes[train_idxs]

        nonrec_val_data = hand_raw_data[val_idxs]
        rec_val_data = hand_rec_data[val_idxs]
        val_targets = hand_targets[val_idxs]
        val_hashes = hand_hashes[val_idxs]

        # TODO named tuple
        val_data = Data(
            nonRec=nonrec_val_data,
            rec=rec_val_data,
            targets=val_targets,
            hashes=val_hashes
        )
        train_data = Data(
            nonRec=np.concatenate((auto_raw_data, nonrec_train_data)),
            rec=np.concatenate((auto_rec_data, rec_train_data)),
            targets=np.concatenate((auto_targets, train_targets)),
            hashes=np.concatenate((auto_hashes, train_hashes))
        )
        validate(model, callbacks, val_data, train_data, i)
        i += 1
        K.clear_session()

def main():
    parser = make_parser()
    args = parser.parse_args()
    # TODO dirty
    if 'func' not in args.__dict__:
        parser.print_usage()
        return
    args.func(vars(args))

if __name__ == '__main__':
    np.random.seed(7)
    main()
