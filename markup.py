#!/usr/bin/env python
# PYTHON_ARGCOMPLETE_OK
import demandimport
with demandimport.enabled():
    import argparse
    import json
    import sys
    import os
    import re
    import argcomplete
    from lib import utils

def make_parser():
    desc = ''
    def is_dir(st):
        if not os.path.isdir(st):
            msg = "Directory '{0}' don't exist".format(st)
            raise argparse.ArgumentTypeError(msg)
        return st

    def is_reg_file(st):
        if not os.path.isfile(st):
            msg = "File '{0}' don't exist".format(st)
            raise argparse.ArgumentTypeError(msg)
        return st

    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument('--config', metavar='config_file', type=is_reg_file, default=os.path.expanduser('~/.estimat.conf'), help="Default - '~/.estimat.conf'")
    subparsers = parser.add_subparsers(help='')

    external_help = 'From *.simple-stat to markup_base'
    parser_external = subparsers.add_parser('external', help=external_help)
    internal_help = 'From markup_base to markup_base'
    parser_internal = subparsers.add_parser('internal', help=internal_help)
    create_help = 'Create markup with labels -- null'
    parser_create = subparsers.add_parser('create', help=create_help)
    stats_help = 'Print some stats from markup_base'
    parser_stats = subparsers.add_parser('stats', help=stats_help)
    cross_compare_help = ''
    parser_cross_compare = subparsers.add_parser('cross-compare', help=cross_compare_help)
    cross_help = 'Print stats about cross-validation'
    parser_cross = subparsers.add_parser('cross', help=cross_help)
    split_help = 'Split markup_base by something'
    parser_split = subparsers.add_parser('split', help=split_help)

    hashes_help = 'If no hashes specified then all hashes from base will be appended'
    parser_internal.add_argument('hashes', metavar='hashes', type=str, nargs='*', help=hashes_help)
    parser_internal.add_argument('--from-project', metavar='from_project_name', type=str, help='Source of reports')
    parser_internal.add_argument('--to-project', metavar='to_project_name', type=str, help='Destination of reports')
    parser_internal.set_defaults(func=internal_command)

    parser_create.add_argument('path-to-diagnostics', metavar='path-to-diagnostics', type=str, nargs='+', help='Path to scan-build diagnostics')
    parser_create.add_argument('--project', metavar='project_name', type=str, help='Destination project')
    parser_create.set_defaults(func=create_command)

    parser_stats.add_argument('--project', metavar='project_name', type=str, help='Project for print stats')
    parser_stats.add_argument('--image', action='store_true', default=False, help='Show image')
    parser_stats.set_defaults(func=stats_command)

    parser_cross_compare.add_argument('--project', metavar='project_name', required=True, type=str, help='Project where was cross-validation')
    parser_cross_compare.add_argument('results', metavar='res file', type=str, nargs='+', help='')
    parser_cross_compare.set_defaults(func=cross_compare_command)

    parser_cross.add_argument('--project', metavar='project_name', required=True, type=str, help='Project where was cross-validation')
    parser_cross.add_argument('--results', metavar='file_with_results', type=str, help='File with cross-validation results')
    parser_cross.set_defaults(func=cross_command)

    markup_file_help = 'Files from scan-view frontend'
    parser_external.add_argument('markup_files', metavar='markup_files', type=is_reg_file, nargs='+', help=markup_file_help)
    parser_external.add_argument('--project', metavar='project_name', type=str, help='Destination project')
    parser_external.set_defaults(func=external_command)

    parser_split.add_argument('--from-project', metavar='from_project_name', type=str, help='Source of reports')
    parser_split.add_argument('--to-project', metavar='to_project_name', type=str, help='Destination of reports')
    parser_split.set_defaults(func=split_command)

    argcomplete.autocomplete(parser)
    return parser

def get_reports_from_diagnostics(paths_to_diagnostics):
    reports = {}
    for d in paths_to_diagnostics:
        if not os.path.isdir(d):
            msg = "'{0}' don't exist or not directory. Maybe you delete this directory?".format(d)
            utils.goodbye(msg)
        for f in os.listdir(d):
            if not re.match(r'^.*[.]simple-stat$', f):
                continue
            with open(os.path.join(d, f)) as fd:
                for i in json.load(fd)['diagnostics']:
                    reports[i['issue_hash_content_of_line_in_context']] = i
    return reports

def create_null_markup(reports, to_file, to_markup_dir):
    to_json = {}
    to_json['targets'] = {}
    with open(to_file) as fd:
        to_json = json.load(fd)

    if len(to_json['targets']):
        msg = 'markup base not empty'
        utils.goodbye(msg)

    for key in reports:
        data_file = key
        with open(os.path.join(to_markup_dir, data_file), 'w') as fd:
            json.dump(reports[key], fd)
        to_json['targets'][key] = {
            'label' : None,
            'data_file' : data_file,
            'mark' : 'auto',
            'predicted_label' : None,
            'TPprob' : None
        }

    with open(to_file, 'w') as fd:
        json.dump(to_json, fd)

def external_join_markup_base(from_files, to_file, to_markup_dir):
    from_jsons = []
    for f in from_files:
        with open(f) as fd:
            from_jsons.append(json.load(fd))
    to_json = {}
    with open(to_file) as fd:
        to_json = json.load(fd)

    reports_dirs = [fj['directory'] for fj in from_jsons]

    reports = get_reports_from_diagnostics(reports_dirs)

    targets = []
    for fj in from_jsons:
        targets.extend(fj['targets'])
    dir_for_save_reports = to_markup_dir
    for target in targets:
        data_file = target['hash']
        with open(os.path.join(dir_for_save_reports, data_file), 'w') as fd:
            json.dump(reports[data_file], fd)
        to_json['targets'][target['hash']] = {
            'label' : target['label'],
            'data_file' : data_file,
            'mark' : 'hand',
            'predicted_label' : None,
            'TPprob' : None
        }

    with open(to_file, 'w') as fd:
        json.dump(to_json, fd)

def internal_join_markup_base(hashes, from_file, from_dir, to_file, to_dir):
    #TODO relative path names
    from_markup_base = None
    with open(from_file) as f:
        from_markup_base = json.load(f)
    if not hashes:
        hashes = set(from_markup_base['targets'].keys())
    to_markup_base = None
    with open(to_file) as f:
        to_markup_base = json.load(f)
    for h in hashes:
        if not from_markup_base['targets'].get(h):
            print("Hash '{0}' not represent in '{1}'".format(h, from_file), file=sys.stderr)
            continue
        file_name = from_markup_base['targets'][h]['data_file']
        to_markup_base['targets'][h] = from_markup_base['targets'][h]
        data = None
        with open(os.path.join(from_dir, file_name)) as f:
            data = json.load(f)
        with open(to_file, 'w') as f:
            json.dump(to_markup_base, f)
        with open(os.path.join(to_dir, file_name), 'w') as f:
            json.dump(data, f)

def split_markup_base(from_file, from_dir, to_file, to_dir, args):
    json_out = {"targets":{}}
    with open(from_file) as ff:
        targets = json.load(ff)['targets']
        for k, target in targets.items():
            with open(os.path.join(from_dir, target['data_file'])) as df:
                data = json.load(df)
                if len(data['path']) == 1:
                    continue
                json_out['targets'][k] = target
                with open(os.path.join(to_dir, target['data_file']), 'w') as odf:
                    json.dump(data, odf)

    with open(to_file, 'w') as tf:
        json.dump(json_out, tf)

def get_destination(options, project):
    if project:
        markup_base, markup_dir = 'project_markup_base', 'project_markup_dir'
    return options[markup_base], options[markup_dir]

def stats_command(arguments):
    import pprint
    import matplotlib.pyplot as plt

    project = arguments['project']
    project_options = utils.readConfigFile(arguments['config'], project)
    markup_base = project_options['project_markup_base']
    with open(markup_base) as f:
        base = json.load(f)
        labels = {}
        for key, target in base['targets'].items():
            label = labels.get(target['label'])
            if label is None:
                labels[target['label']] = 1
                continue
            labels[target['label']] += 1

        f = lambda x: x.get('TPprob') is not None and x.get('predicted_label') is not None
        targets = list(filter(f, base['targets'].values()))
        targets.sort(key=lambda x: (utils.getSortKey(x['TPprob']), x['data_file']), reverse=True)
        pl = []
        tp = 0
        for i in range(len(targets)):
            target = targets[i]
            if int(target['label']) in (-2, -1):
                tp += 1
            pl.append(tp / (i + 1))

        pprint.pprint(sum(labels.values()))
        pprint.pprint(labels)
        if arguments['image']:
            plt.plot(pl)
            plt.show()

def cross_compare_command(arguments):
    import matplotlib as mpl
    import matplotlib.pyplot as plt
    import seaborn as sns
    import pprint
    from random import shuffle

    sns.set(font_scale=3)
    sns.set_style("whitegrid")
    sns.set_palette(sns.color_palette("Set2", 10))

    project = arguments['project']
    project_options = utils.readConfigFile(arguments['config'], project)
    markup_base = project_options['project_markup_base']
    base = None
    res = None
    plt.title('True positive rate')
    plt.ylabel('True positive / warnings', style='italic')
    plt.xlabel('warnings', style='italic')
    fig = plt.figure(1)
    ax = fig.add_subplot(111)
    with open(markup_base) as f:
        base = json.load(f)
    for result, l in zip(arguments['results'], ['С использованием базовых признаков', 'С использованием всех признаков']):
        with open(result) as f:
            res = json.load(f)
        targets = []
        for piece in res['results']:
            for key, value in piece['self'].items():
                targets.append((key, value))
        targets.sort(key=lambda x: (utils.getSortKey(utils.probs_to_TPprobs(x[1]['probs'])), x[0]), reverse = True)
        pl = []
        tp = 0
        for i in range(len(targets)):
            target = base['targets'][targets[i][0]]
            if int(target['label']) in (-2, -1):
                tp += 1
            pl.append(tp / (i + 1))
        ax.plot(pl, linewidth=4, label=l)
    targets = []
    for key, target in base['targets'].items():
        if target['mark'] == 'hand':
            targets.append(target)
    targets.sort(key=lambda x: int(x['label']))
    pl = []
    tp = 0
    for i in range(len(targets)):
        target = targets[i]
        if int(target['label']) in (-2, -1):
            tp += 1
        pl.append(tp / (i + 1))
    ax.plot(pl, linestyle='-', linewidth=2, label='Идеальное ранжирование') # ideal
    shuffle(targets)
    pl = []
    tp = 0
    for i in range(len(targets)):
        target = targets[i]
        if int(target['label']) in (-2, -1):
            tp += 1
        pl.append(tp / (i + 1))
    ax.plot(pl, linewidth=2, linestyle='-', label='Случайное ранжирование') # random
    handles, labels = ax.get_legend_handles_labels()
    ax.legend(handles, labels, loc=3)
    plt.show()

def cross_command(arguments):
    import matplotlib as mpl
    import matplotlib.pyplot as plt
    import seaborn as sns
    import pprint

    sns.set(font_scale=3)
    sns.set_style("whitegrid")
    sns.set_palette(sns.color_palette("Set2", 10))

    project = arguments['project']
    project_options = utils.readConfigFile(arguments['config'], project)
    markup_base = project_options['project_markup_base']
    base = None
    res = None
    with open(markup_base) as f:
        base = json.load(f)
    with open(arguments['results']) as f:
        res = json.load(f)
    get_medium = lambda key, all_key : sum(i[key] / i[all_key] for i in res['results']) / len(res['results'])
    medium_correct = get_medium('correct', 'length')
    medium_almost_correct = get_medium('almost_correct', 'length')
    medium_other_incorrect = get_medium('other_incorrect', 'other_amount')
    pprint.pprint("Correct : {0}".format(medium_correct))
    pprint.pprint("Almost correct : {0}".format(medium_almost_correct))
    pprint.pprint("Fp-like on TP-like: {0}".format(medium_other_incorrect))
    targets = []
    for piece in res['results']:
        for key, value in piece['self'].items():
            targets.append((key, value))
    targets.sort(key=lambda x: (utils.getSortKey(utils.probs_to_TPprobs(x[1]['probs'])), x[0]), reverse = True)
    pl = []
    tp = 0
    for i in range(len(targets)):
        target = base['targets'][targets[i][0]]
        if int(target['label']) in (-2, -1):
            tp += 1
        pl.append(tp / (i + 1))
    plt.title('True positive rate')
    plt.ylabel('True positive / warnings', style='italic')
    plt.xlabel('warnings', style='italic')
    plt.plot(pl, linestyle='-', linewidth=4, label='true posirive rate')
    plt.show()
    plt.close()
    get_hist_section = lambda key : list(map(lambda x : x['history'][key], res['results']))
    loss = get_hist_section('loss')
    val_loss = get_hist_section('val_loss')
    plt.title('Loss function')
    plt.ylabel('loss', style='italic')
    plt.xlabel('epoch', style='italic')
    labels = []
    fig = plt.figure(1)
    ax = fig.add_subplot(111)
    ax.plot([sum(i) / len(res['results']) for i in zip(*get_hist_section('loss'))], linestyle='-', linewidth=5, label='loss')
    ax.plot([sum(i) / len(res['results']) for i in zip(*get_hist_section('val_loss'))], linestyle='-', linewidth=5, label='validation loss')
    ax.plot([max(i) for i in zip(*val_loss)], linestyle='--', color=sns.color_palette()[2], linewidth=2, label='min-max validation loss')
    ax.plot([min(i) for i in zip(*val_loss)], linestyle='--', color=sns.color_palette()[2], linewidth=2)
    ax.plot([max(i) for i in zip(*loss)], linestyle='--', color=sns.color_palette()[3], linewidth=2, label='min-max loss')
    ax.plot([min(i) for i in zip(*loss)], linestyle='--', color=sns.color_palette()[3], linewidth=2)
    handles, labels = ax.get_legend_handles_labels()
    ax.legend(handles, labels)
    plt.show()

def create_command(arguments):
    project = arguments['project']
    project_options = utils.readConfigFile(arguments['config'], project)
    reports = get_reports_from_diagnostics(arguments['path-to-diagnostics'])
    to_file, to_dir = get_destination(project_options, project)
    create_null_markup(reports, to_file, to_dir)

def external_command(arguments):
    project = arguments['project']
    project_options = utils.readConfigFile(arguments['config'], project)
    from_files = arguments['markup_files']
    to_file, to_dir = get_destination(project_options, project)
    external_join_markup_base(from_files, to_file, to_dir)

def internal_command(arguments):
    from_project = arguments['from_project']
    to_project = arguments['to_project']
    from_project_options = utils.readConfigFile(arguments['config'], from_project)
    to_project_options = utils.readConfigFile(arguments['config'], to_project)
    to_file, to_dir = get_destination(to_project_options, to_project)
    from_file, from_dir = get_destination(from_project_options, from_project)
    hashes = arguments['hashes']
    internal_join_markup_base(hashes, from_file, from_dir, to_file, to_dir)

def split_command(arguments):
    from_project = arguments['from_project']
    to_project = arguments['to_project']
    from_project_options = utils.readConfigFile(arguments['config'], from_project)
    to_project_options = utils.readConfigFile(arguments['config'], to_project)
    to_file, to_dir = get_destination(to_project_options, to_project)
    from_file, from_dir = get_destination(from_project_options, from_project)
    split_markup_base(from_file, from_dir, to_file, to_dir, arguments)

def main():
    parser = make_parser()
    args = parser.parse_args()
    args.func(vars(args))

if __name__ == '__main__':
    main()
