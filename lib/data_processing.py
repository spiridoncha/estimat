import demandimport
with demandimport.enabled():
    import json
    import os
    import sys
    import numpy as np
    from lib import utils

def getDataFromBase(project_markup_base, project_markup_dir):
    def get_data_from_markup_base(markup_base, markup_dir):
        reports = []
        targets = []
        with open(markup_base) as f:
            markup = json.load(f)['targets']
            for report_hash in markup:
                entry = markup[report_hash]
                label = entry['label']
                mark = entry['mark'] if 'mark' in entry else 'auto'
                p_label = entry['predicted_label'] if 'predicted_label' in entry else None
                tp_prob = entry['TPprob'] if 'TPprob' in entry else None
                data_file = entry['data_file']
                targets.append({
                    'label' : label,
                    'mark' : mark,
                    'predicted_label' : p_label,
                    'TPprob' : tp_prob
                })
                with open(os.path.join(markup_dir, data_file)) as df:
                    reports.append(json.load(df))
        return reports, targets

    data, targets = get_data_from_markup_base(project_markup_base, project_markup_dir)
    return data, targets

def getTargetsFromLabels(labels, output_dim, targets_null):
    def transform(target, to_dim):
        label = target['label']
        a = np.array([0] * to_dim)
        if label is None:
            print("Warning: label null", file=sys.stderr)
            return a
        a[int(label) + 2] = 1
        return a

    return [transform(t, output_dim) for t in labels] if not targets_null else labels

def rawToNP(raw_data, raw_targets, checkers_base, output_dim, targets_null=False):
    from keras.preprocessing.sequence import pad_sequences

    data, targets = raw_data, raw_targets
    data, targets = [get_need_data(entry) for entry in data], getTargetsFromLabels(targets, output_dim, targets_null)

    with utils.open_checkers_base(checkers_base) as base:
        data = [entryToFeatures(entry, base) for entry in data]

    data, rec_data = [d[0] for d in data], [d[1] for d in data]
    rec_data = pad_sequences(rec_data, maxlen=100)
    data, rec_data, targets = np.array(data), np.array(rec_data), np.array(targets)
    return data, rec_data, targets

def get_need_data(entry):
    def get_need_data_from_piece(piece):
        stmt_class_name = piece.get('stmt_class_name')
        depth = piece.get('depth')
        res = {}
        res['stmt_class_name'] = stmt_class_name if stmt_class_name else ""
        res['kind'] = piece['kind']
        res['depth'] = depth if depth else 0
        res['constraints'] = piece.get('constraints')
        res['bindings'] = piece.get('bindings')
        res['environment'] = piece.get('environment')
        res['condition'] = piece.get('condition')
        res['names'] = piece.get('names')
        res['nonExprNames'] = piece.get('nonExprNames')
        return res

    res = {}
    full_copy_keys = ['check_name', 'description', 'category', 'issue_context_kind', 'type']
    for key in full_copy_keys:
        res[key] = entry[key]
    path = entry['path']
    res['path_length'] = len(path)
    res['amount_of_event'] = len([i for i in path if i['kind'] == 'event'])
    res['amount_of_control'] = len([i for i in path if i['kind'] == 'control'])
    control_path = list(filter(lambda x: x.get('depth'), path))
    res['amount_of_function_change'] = 0 if res['path_length'] == 0 else sum([abs(i['depth'] - j['depth']) for (i, j) in zip(control_path, control_path[1:])])
    res['depth'] = 0 if res['path_length'] == 0 else path[-1]['depth'] - min([piece['depth'] for piece in control_path])
    # res['error_stmt'] = "" if res['path_length'] == 0 else path[-1]['stmt_class_name'] if path[-1].get('stmt_class_name') else ""
    res['path'] = [get_need_data_from_piece(piece) for piece in path]
    return res

def have_contradiction_var(path):
    def have_store(piece, var):
        return var in set(piece['names'].values()) | set(piece['nonExprNames'].values())

    def is_opposite_ops(op1, op2):
        opposite_ops = {
            '==' : '!=',
            '!=' : '==',
            '<'  : '>=',
            '>='  : '<',
            '>'  : '<=',
            '<='  : '>',
        }
        return opposite_ops[op1] == op2

    conditions = {}
    for piece in path:
        if piece['condition'] is None:
            continue
        condition = piece['condition']
        var = condition['variable']
        op  = condition['operation']
        val = condition['value']
        if have_store(piece, var):
            continue
        if var not in conditions:
            conditions[var] = { "op" : op, "val": val }
            continue
        old_condition = conditions[var]
        new_condition = { "op" : op, "val" : val }
        if is_opposite_ops(new_condition["op"], old_condition["op"]) and new_condition['val'] == old_condition['val']:
            return [1]
    return [0]

def have_strange_var_in_path(path):
    constraints = {}
    for piece in path:
        if piece['constraints'] is None:
            continue
        for key, value in piece['constraints'].items():
            if key not in constraints:
                constraints[key] = value
                continue
            if constraints[key] != value:
                if key not in piece['bindings']:
                    return [1]
    return [0]

def have_lost_constraint(path):
    constraints = set([])
    for piece in path:
        if piece['constraints'] is None:
            continue
        for i in constraints:
            if i not in piece['constraints']:
                return [1]
        constraints |= piece['constraints'].keys()
    return [0]

def have_constraint_for_global_var(path):
    for piece in path:
        if piece['bindings'] is None:
            continue
        if piece['constraints'] is None:
            continue
        global_vars = [k for k, v in piece['bindings'].items() if v['global']]
        for i in global_vars:
            if i in piece['constraints']:
                return [1]
    return [0]

def get_env_stat(env, base):
    if env in base['data']:
        return [100 * base['data'][env] / base['amount']]
    return [0]

def entryToFeatures(entry, checkers_base):
    def updateBase(base, data, border):
        if base['data'].get(data) is not None:
            return
        base['data'][data] = base['next']
        base['next'] += 1
        if base['next'] > border:
            msg = "max amount of ?? is {0}".format(border)
            utils.goodbye(msg)

    def textualToVector(txt_data, base, border=30):
        updateBase(base, txt_data, border)
        res = [0] * border
        res[base['data'][txt_data]] = 1
        return res

    res = []
    res_recurrent = []
    res.append(entry['path_length'])
    # res.append(entry['amount_of_event'])
    # res.append(entry['amount_of_control'])
    res.append(entry['amount_of_function_change'])
    res.append(entry['depth'])
    res.extend(textualToVector(entry['check_name'], checkers_base['checkers'], 20))
    res.extend(textualToVector(entry['category'], checkers_base['categories'], 7))
    res.extend(get_env_stat(entry['path'][-1]['environment'], checkers_base['env_stat']))
    res.extend(have_contradiction_var(entry['path']))
    res.extend(have_strange_var_in_path(entry['path']))
    res.extend(have_lost_constraint(entry['path']))
    res.extend(have_constraint_for_global_var(entry['path']))
    res.extend(textualToVector(entry['path'][-1]['stmt_class_name'], checkers_base['stmts'], 32))
    # res.extend(textualToVector(entry['type'], checkers_base['types'], 17))
    for piece in entry['path']:
        piece_part = []
        piece_part.append(piece['depth'])
        piece_part.extend(textualToVector(piece['stmt_class_name'], checkers_base['stmts'], 32))
        piece_part.extend(textualToVector(piece['kind'], checkers_base['piece_kinds'], 2))
        res_recurrent.append(piece_part)
    return res, res_recurrent
