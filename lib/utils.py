import demandimport
with demandimport.enabled():
    import sys
    from contextlib import contextmanager
    import os
    import json

def goodbye(msg):
    print(msg, file=sys.stderr)
    exit(1)

def readConfigFile(config_file, project=None):
    project_markup_base = 'markup_base'
    project_markup_dir = 'markup_dir'

    def isFile(f):
        if not os.path.isfile(f):
            msg = ' '.join(
                [
                    "'{0}' don't exist or not regular file.".format(f),
                ]
            )
            goodbye(msg)

    def isDir(d):
        if not os.path.isdir(d):
            msg = ' '.join(
                [
                    "'{0}' don't exist or not directory.".format(d),
                ]
            )
            goodbye(msg)

    def checkSection(config, section):
        if section not in config:
            msg = "You must define '{0}' section in config ".format(section)
            goodbye(msg)

    def checkSectionVariable(section, variable):
        if variable not in section:
            msg = "You must define '{0}' in section '{1}'".format(variable, section)
            goodbye(msg)

    def checkProjectSection(config, project):
        checkSection(config, project)
        project_section = config[project]
        project_section_vars = [project_markup_base, project_markup_dir]
        for i in project_section_vars:
            checkSectionVariable(project_section, i)
        isFile(project_section[project_markup_base])
        isDir(project_section[project_markup_dir])

    def checkDefaultSection(config):
        checkSection(config, 'DEFAULT')
        default_section = config['DEFAULT']
        default_section_vars = ['checkers_base']
        for i in default_section_vars:
            checkSectionVariable(default_section, i)
        #TODO I can create initially base automaticly
        isFile(default_section['checkers_base'])

    config = None
    with open(config_file) as fd:
        config = json.load(fd)
    res = {}
    checkDefaultSection(config)
    res['checkers_base'] = config['DEFAULT']['checkers_base']
    if project:
        checkProjectSection(config, project)
        res['project_markup_base'] = config[project][project_markup_base]
        res['project_markup_dir'] = config[project][project_markup_dir]
    return res

@contextmanager
def open_checkers_base(filename):
    base = None
    with open(filename, 'r') as f:
        base = json.load(f)
    yield base
    with open(filename, 'w') as f:
        json.dump(base, f)

def getAlpha():
    # constant from learning
    return 1.0

def getSortKey(probs):
    alpha = getAlpha()
    return max(probs['TP'], alpha * probs['MTP'])

def probs_to_TPprobs(probs):
    return {
        'TP'   : probs[0],
        'MTP'  : probs[1],
        'UNK'  : probs[2],
        'FP'   : probs[3]
    }
