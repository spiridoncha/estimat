#!/usr/bin/env python
# PYTHON_ARGCOMPLETE_OK
import demandimport
with demandimport.enabled():
    import argparse
    import json
    import os
    import argcomplete
    from lib import utils

def make_parser():
    desc = ''
    def is_dir(st):
        if not os.path.isdir(st):
            msg = "Directory '{0}' don't exist".format(st)
            raise argparse.ArgumentTypeError(msg)
        return st

    def is_reg_file(st):
        if not os.path.isfile(st):
            msg = "File '{0}' don't exist".format(st)
            raise argparse.ArgumentTypeError(msg)
        return st

    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument('--config', metavar='config_file', type=is_reg_file, default=os.path.expanduser('~/.estimat.conf'), help="Default - '~/.estimat.conf'")
    parser.add_argument('stat_files', metavar='stat_files', type=is_reg_file, nargs='+', help='Files with env-stat')
    parser.set_defaults(func=make_stat)

    argcomplete.autocomplete(parser)
    return parser

def make_stat(args):
    project_options = utils.readConfigFile(args['config'])
    with utils.open_checkers_base(project_options['checkers_base']) as checkers_base:
        env_base = checkers_base['env_stat']
        for file_name in args['stat_files']:
            with open(file_name) as f:
                for s in f:
                    key = s.rstrip()
                    if key not in env_base['data']:
                        env_base['data'][key] = 1
                    else:
                        env_base['data'][key] = env_base['data'][key] + 1
                    env_base['amount'] = env_base['amount'] + 1

def main():
    parser = make_parser()
    args = parser.parse_args()
    args.func(vars(args))

if __name__ == '__main__':
    main()
